export default class FizzBuzz {
  public generate(minNumber: number, maxNumber: number): string {
    let result = '';

    while (minNumber <= maxNumber) {
      result += this.determineFizzBuzzString(minNumber);
      minNumber++;
    }

    return result;
  }

  private determineFizzBuzzString(number: number): string {
    if (number % 15 === 0) return 'fizzbuzz';
    if (number % 3 === 0) return 'fizz';
    if (number % 5 === 0) return 'buzz';
    return number.toString(10);
  }
}
