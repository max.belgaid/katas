import FizzBuzz from './FizzBuzz';

describe('FizzBuzz', () => {
  it('should return "1" if number is 1', () => {
    expect(new FizzBuzz().generate(1, 1)).toEqual('1');
  });

  it('should return "2" if number is 2', () => {
    expect(new FizzBuzz().generate(2, 2)).toEqual('2');
  });

  it('should return "fizz" if number is 3', () => {
    expect(new FizzBuzz().generate(3, 3)).toEqual('fizz');
  });

  it('should return "buzz" if number is 5', () => {
    expect(new FizzBuzz().generate(5, 5)).toEqual('buzz');
  });

  it('should return "fizz" if number is 6', () => {
    expect(new FizzBuzz().generate(6, 6)).toEqual('fizz');
  });

  it('should return "buzz" if number is 10', () => {
    expect(new FizzBuzz().generate(10, 10)).toEqual('buzz');
  });

  it('should return "fizzbuzz" if number is 15', () => {
    expect(new FizzBuzz().generate(15, 15)).toEqual('fizzbuzz');
  });

  it('should return "fizzbuzz" if number is 30', () => {
    expect(new FizzBuzz().generate(30, 30)).toEqual('fizzbuzz');
  });

  it('should return "12" if numbers are 1 and 2', () => {
    expect(new FizzBuzz().generate(1, 2)).toEqual('12');
  });

  it('should return "12fizz" if numbers are 1 to 3', () => {
    expect(new FizzBuzz().generate(1, 3)).toEqual('12fizz');
  });

  it('should return "12fizz4buzz" if numbers are 1 to 5', () => {
    expect(new FizzBuzz().generate(1, 5)).toEqual('12fizz4buzz');
  });

  it('should return "12fizz4buzzfizz78fizzbuzz11fizz1314fizzbuzz" if numbers are 1 to 15', () => {
    expect(new FizzBuzz().generate(1, 15)).toEqual('12fizz4buzzfizz78fizzbuzz11fizz1314fizzbuzz');
  });
});
