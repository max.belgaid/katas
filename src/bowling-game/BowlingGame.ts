export default class BowlingGame {
  private _score = 0;
  private rolls: number[] = [];
  private currentRoll = 0;

  public roll(pins: number) {
    this.rolls[this.currentRoll++] = pins;
  }

  public computeScore() {
    let frameIndex = 0;
    for (let frame = 0; frame < 10; frame++) {
      if (this.isStrike(frameIndex)) {
        this._score += 10 + this.computeStrikeBonus(frameIndex);
        frameIndex++;
      } else if (this.isSpare(frameIndex)) {
        this._score += 10 + this.computeSpareBonus(frameIndex);
        frameIndex += 2;
      } else {
        this._score += this.rolls[frameIndex] + this.rolls[frameIndex + 1];
        frameIndex += 2;
      }
    }
    return this._score;
  }

  private isStrike(frameIndex: number) {
    return this.rolls[frameIndex] == 10;
  }

  private computeStrikeBonus(frameIndex: number) {
    return this.rolls[frameIndex + 1] + this.rolls[frameIndex + 2];
  }

  private isSpare(frameIndex: number) {
    return this.rolls[frameIndex] + this.rolls[frameIndex + 1] == 10;
  }

  private computeSpareBonus(frameIndex: number) {
    return this.rolls[frameIndex + 2];
  }
}
