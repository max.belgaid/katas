import BowlingGame from './BowlingGame';

describe('Bowling game', () => {
  let game: BowlingGame;

  function rollSeveralBalls(times: number, fallingPins: number) {
    for (let i = 0; i < times; i++) {
      game.roll(fallingPins);
    }
  }

  function rollASpare() {
    rollSeveralBalls(2, 5);
  }

  function rollAStrike() {
    game.roll(10);
  }

  beforeEach(() => {
    game = new BowlingGame();
  });

  it('playing a gutter game should score 0', () => {
    rollSeveralBalls(20, 0);

    expect(game.computeScore()).toBe(0);
  });

  it('hitting 1 pin on each roll should score 20', () => {
    rollSeveralBalls(20, 1);

    expect(game.computeScore()).toBe(20);
  });

  it("hitting a spare should add a the next roll's score to the frame", () => {
    rollASpare();
    game.roll(7);
    rollSeveralBalls(17, 0);

    expect(game.computeScore()).toBe(24);
  });

  it("hitting a strike should add the next two rolls' to the score of the current frame", () => {
    rollAStrike();
    game.roll(2);
    game.roll(3);
    rollSeveralBalls(16, 0);

    expect(game.computeScore()).toBe(20);
  });

  it('playing a perfect game should score 300', () => {
    rollSeveralBalls(12, 10);

    expect(game.computeScore()).toBe(300);
  });
});
