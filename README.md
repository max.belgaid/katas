# Katas

This Git repository was created to implement code katas to practice development skills like TDD, Clean Architecture or
any other practice I find interesting.

The tooling of the project is simple:

- TypeScript
- Jest as test runner
- ESLint as linter
- lint-staged and Husky to run tests and lint on commit.
- Yarn as package manager

## Getting started

To install dependencies:

```shell
yarn install
```

To run tests:

```shell
yarn test
```
